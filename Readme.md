# Socksbalance

Socksbalance is a **socks** proxy that does load **balanc**ing.

For now, it does this for IPv6 only, where a client knows its public
IP addresses. IPv4 traffic continues taking the default route.

Socksbalance supports parts of [SOCKS Protocol Version 5](https://datatracker.ietf.org/doc/html/rfc1928).
It does only support the ``NO_AUTHENTICATION_REQUIRED`` authentication and only the
``CONNECT`` command.

## Usage

``cargo run`` (for launching directly; see next section for the installation)

The proxy server is listening at port 1080 on the IPv6 loopback interface (``::1``) only.

## Installation

```
cargo build --release
sudo install target/release/socksbalance /usr/local/bin
sudo useradd -M socksbalance
sudo tee /etc/systemd/system/socksbalance.service <<"EOF"
[Unit]
Description=Socksbalance load balancing proxy server

[Service]
ExecStart=/usr/local/bin/socksbalance
Type=notify
User=socksbalance
Grpup=socksbalance

[Install]
WantedBy=multi-user.target
EOF
sudo systemctl daemon-reload
sudo systemctl enable socksbalance.service
sudo systemctl restart socksbalance.service
```

## Setup in Applications

### apt

See the manpages ``apt.conf`` and ``apt-transport-http`` for details.

```
sudo tee /etc/apt/apt.conf.d/80socksbalance <<"EOF"
Acquire::http::Proxy "socks5h://::1:1080";
EOF
```

**Important:** ``Acquire::Queue-Mode`` does not support more than one connection per host
which can limit your speedup or result in no speedup at all when apt is the only application
that downloads something.

### curl

This can be used for checking that all your IPs are used by sending multiple requests.

```
ALL_PROXY=socks5://[::1]:1080 curl https://ipv6.jsonip.com/
```

### pacman

Edit ``/etc/pacman.conf`` and extend the ``[options]`` section:

```
ParallelDownloads = 4
```

Then use ``sudo ALL_PROXY=socks5://[::1]:1080 pacman -Syu`` for updates.

## License

socksbalance - a load balancing proxy  
Copyright (C) 2022 Jonas Lochmann

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
