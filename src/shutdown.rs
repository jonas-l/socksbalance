use tokio::sync::{mpsc, watch};

pub struct ShutdownSender {
    inform_still_running_waiter: mpsc::Receiver<()>,
    signal_sender: watch::Sender<Option<ShutdownRequest>>,
}

#[derive(Clone)]
pub struct ShutdownReceiver {
    _inform_still_running_notifier: mpsc::Sender<()>,
    signal_receiver: watch::Receiver<Option<ShutdownRequest>>,
}

enum ShutdownRequest {
    Graceful,
    Force,
}

pub fn setup_shutdown_system() -> (ShutdownSender, ShutdownReceiver) {
    let (signal_sender, signal_receiver) = watch::channel::<Option<ShutdownRequest>>(None);

    // this is never used for sending anything
    let (inform_still_running_notifier, inform_still_running_waiter) = mpsc::channel(1);

    let sender = ShutdownSender { inform_still_running_waiter, signal_sender };

    let receiver = ShutdownReceiver {
        _inform_still_running_notifier: inform_still_running_notifier,
        signal_receiver,
    };

    (sender, receiver)
}

impl ShutdownSender {
    pub async fn shutdown_graceful(&mut self) {
        if self.signal_sender.borrow().is_none() {
            let _ = self.signal_sender.send(Some(ShutdownRequest::Graceful));
        }

        self.wait_for_shutdown().await;
    }

    pub async fn force_shutdown(&mut self) {
        // if it fails, everything shut down already; just ignore it
        let _ = self.signal_sender.send(Some(ShutdownRequest::Force));

        self.wait_for_shutdown().await;
    }

    async fn wait_for_shutdown(&mut self) {
        assert_eq!(self.inform_still_running_waiter.recv().await, None);
    }
}

impl ShutdownReceiver {
    pub async fn wait_for_graceful_shutdown(&mut self) {
        loop {
            if let Some(_) = *self.signal_receiver.borrow_and_update() {
                break
            }

            if self.signal_receiver.changed().await.is_err() {
                // the sender was dropped so it's time
                break
            }
        }
    }

    pub async fn wait_for_forced_shutdown(&mut self) {
        loop {
            if let Some(ShutdownRequest::Force) = *self.signal_receiver.borrow_and_update() {
                break
            }

            if self.signal_receiver.changed().await.is_err() {
                // the sender was dropped so it's time
                break
            }
        }
    }
}
