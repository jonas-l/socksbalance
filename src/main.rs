use std::net::{SocketAddr, Ipv6Addr, SocketAddrV6};
use sd_notify::NotifyState;
use clap::Parser;
use log::ConsoleLogger;
use server::ServerConfig;

mod ip;
mod log;
mod server;
mod socks;
mod shutdown;

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let params = Parameters::parse();

    let config = ServerConfig {
        bind: SocketAddr::V6(SocketAddrV6::new(Ipv6Addr::LOCALHOST, 1080, 0, 0)),
        logger: ConsoleLogger {},
        max_connections: params.max_connections,
        verbose_logging: params.enable_verbose_logging,
    };

    let mut server = match config.launch().await {
        Ok(v) => v,
        Err(_) => std::process::exit(1),
    };

    send_systemd_notification(NotifyState::Ready);

    tokio::signal::ctrl_c().await.unwrap();

    send_systemd_notification(NotifyState::Stopping);

    tokio::select! {
        _ = server.shutdown_graceful() => {},
        _ = tokio::signal::ctrl_c() => {},
    };

    server.force_shutdown().await;
}

fn send_systemd_notification(state: NotifyState) {
    match sd_notify::notify(false, &[state]) {
        Ok(_) => {},
        Err(err) => eprintln!("sd_notify failed: {}", err),
    }
}

#[derive(Parser)]
#[clap(author, version, about)]
struct Parameters {
    #[clap(short = 'v', help = "Enable verbose logging")]
    enable_verbose_logging: bool,
    #[clap(short = 'c', help = "Maximum currecnt connections", default_value_t = 8096)]
    max_connections: usize,
}
