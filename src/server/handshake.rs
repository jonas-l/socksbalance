use tokio::io::{Error, ErrorKind, AsyncRead, AsyncWrite, AsyncWriteExt};
use crate::socks::{ authentication, SocksClientHello, SocksServerHello, SocksClientRequest };
use crate::log::Logger;

pub async fn do_handshake<T: Logger, S: AsyncRead + AsyncWrite + Unpin>(mut socket: &mut S, logger: &T) -> Result<SocksClientRequest, Error> {
    let client_hello = SocksClientHello::decode(&mut socket).await?;

    if client_hello.methods.iter().find(|method| **method == authentication::NO_AUTHENTICATION_REQUIRED) == None {
        SocksServerHello { method: authentication::NO_ACCEPTABLE_METHODS }.encode(&mut socket).await?;

        return Err(Error::new(ErrorKind::InvalidData, "client does not support the NO_AUTHENTICATION_REQUIRED authentication"));
    }

    logger.log(format_args!("got client hello: {:?}", client_hello));

    let server_hello = SocksServerHello { method: authentication::NO_AUTHENTICATION_REQUIRED };

    server_hello.encode(&mut socket).await?;
    socket.flush().await?;

    logger.log(format_args!("send server hello: {:?}", server_hello));

    let client_request = SocksClientRequest::decode(&mut socket).await?;

    logger.log(format_args!("got client request: {:?}", client_request));

    Ok(client_request)
}
