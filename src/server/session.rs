use std::io::ErrorKind;
use tokio::io::{copy_bidirectional, AsyncWriteExt, BufStream, AsyncRead, AsyncWrite};
use tokio::net::TcpStream;
use crate::socks::{ SocksClientRequestCommand, SocksServerResponse, SocksEndpoint, SocksServerReply };
use crate::ip::get_public_ipv6_addresses;
use crate::log::Logger;
use super::connect::{connect_to_socket_addrs, ConnectOptions};
use super::handshake::do_handshake;

pub async fn handle_session<T: Logger>(socket: TcpStream, request_index: u16, logger: &T) {
    let mut socket = BufStream::new(socket);

    let client_request = match do_handshake(&mut socket, logger).await {
        Ok(v) => v,
        Err(err) => {
            logger.log(format_args!("handshake failed: {}", err));

            return
        },
    };

    match client_request.command {
        SocksClientRequestCommand::Connect => {
            let addrs = match client_request.endpoint.to_socket_addrs().await {
                Ok(v) => v,
                Err(err) => {
                    logger.log(format_args!("endpoint resolution failed: {}", err));

                    send_response_log_and_ignore_error(
                        socket,
                        logger,
                        &SocksServerResponse{
                            reply: SocksServerReply::GeneralServerFailure,
                            endpoint: SocksEndpoint::default(),
                        }
                    ).await;

                    return
                }
            };

            logger.log(format_args!("client wants to connect to {:?}", addrs));

            let current_ips = get_public_ipv6_addresses();

            let bind_ipv6_addr = if current_ips.len() >= 2 {
                Some(current_ips[usize::from(request_index) % current_ips.len()])
            } else {
                None
            };

            let options = ConnectOptions { bind_ipv6_addr };

            let mut target_stream = match connect_to_socket_addrs(addrs.as_slice(), &options).await {
                Ok(v) => v,
                Err(err) => {
                    logger.log(format_args!("Could not connect to the target: {:?}: {}", err.kind(), err));

                    send_response_log_and_ignore_error(
                        socket,
                        logger,
                        &SocksServerResponse{
                            reply: match err.kind() {
                                ErrorKind::ConnectionRefused => SocksServerReply::HostUnreachable,
                                _ => SocksServerReply::GeneralServerFailure,
                            },
                            endpoint: SocksEndpoint::default(),
                        }
                    ).await;

                    return
                },
            };

            let local_addr = match target_stream.local_addr() {
                Ok(v) => v,
                Err(err) => {
                    logger.log(format_args!("could not get local address: {}", err));

                    return
                },
            };

            match (SocksServerResponse{
                reply: SocksServerReply::Succeeded,
                endpoint: local_addr.into(),
            }.encode(&mut socket).await) {
                Ok(_) => {/* ignore */},
                Err(err) => {
                    logger.log(format_args!("could not send succeeded response: {}", err));

                    return
                },
            };

            // errors are ignored starting this point
            logger.log(format_args!("starting traffic forwarding"));

            let _ = socket.flush().await;
            let mut socket = socket.into_inner();   // disable buffering

            let _ = copy_bidirectional(&mut target_stream, &mut socket).await;
        }
        command => {
            logger.log(format_args!("client wanted {:?} but this is not implemented", command));

            send_response_log_and_ignore_error(
                socket,
                logger,
                &SocksServerResponse{
                    reply: SocksServerReply::CommandNotSupported,
                    endpoint: SocksEndpoint::default(),
                }
            ).await
        }
    };
}

async fn send_response_log_and_ignore_error<T: Logger, S: AsyncRead + AsyncWrite>(socket: S, logger: &T, response: &SocksServerResponse) {
    tokio::pin!(socket);

    match response.encode(&mut socket).await {
        Ok(_) => {/* nothing to do */},
        Err(err) => logger.log(format_args!("could not send response: {}", err)),
    }
}
