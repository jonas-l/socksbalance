use std::net::{SocketAddr, SocketAddrV6, Ipv6Addr};
use tokio::io::{Error, ErrorKind};
use tokio::net::{TcpStream, TcpSocket, ToSocketAddrs, lookup_host};

pub struct ConnectOptions {
    pub bind_ipv6_addr: Option<Ipv6Addr>,
}

pub async fn connect_to_socket_addrs<T: ToSocketAddrs>(targets: T, options: &ConnectOptions) -> Result<TcpStream, Error> {
    let mut last_error = None;

    for addr in lookup_host(targets).await? {
        match connect_to_socket_addr(&addr, options).await {
            Ok(socket) => return Ok(socket),
            Err(err) => last_error = Some(err),
        }
    }

    Err(match last_error {
        Some(error) => error,
        None => Error::new(ErrorKind::NotFound, "no target addresses found"),
    })
}

async fn connect_to_socket_addr(target: &SocketAddr, options: &ConnectOptions) -> Result<TcpStream, Error> {
    Ok(match target {
        SocketAddr::V4 (_) => {
            let socket = TcpSocket::new_v4()?;

            socket.connect(*target).await?
        },
        SocketAddr::V6 (_) => {
            let socket = TcpSocket::new_v6()?;

            if let Some(bind_addr) = options.bind_ipv6_addr {
                socket.bind(SocketAddr::V6(SocketAddrV6::new(bind_addr, 0, 0, 0)))?;
            }

            socket.connect(*target).await?
        },
    })
}
