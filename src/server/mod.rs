use std::sync::Arc;
use std::net::SocketAddr;
use tokio::io::Error;
use tokio::net::TcpListener;
use tokio::sync::Semaphore;
use tokio::task::spawn;
use crate::log::{Logger, PrefixLogger, NullLogger};
use crate::shutdown::{setup_shutdown_system, ShutdownSender};
use session::handle_session;

mod connect;
mod handshake;
mod session;

pub struct ServerConfig<L: Logger> {
    pub bind: SocketAddr,
    pub logger: L,
    pub verbose_logging: bool,
    pub max_connections: usize,
}

impl <L: Logger + Send + Sync + 'static> ServerConfig<L> {
    pub async fn launch(self) -> Result<ShutdownSender, Error> {
        let listener = match TcpListener::bind(self.bind).await {
            Ok(listener) => listener,
            Err(err) => {
                self.logger.log(format_args!("Could not bind: {}", err));

                return Err(err)
            }
        };

        let worker_semaphore = Arc::new(Semaphore::new(self.max_connections));
        let (shutdown_sender, mut shutdown_receiver) = setup_shutdown_system();
        let worker_shutdown_receiver = shutdown_receiver.clone();

        spawn(async move {
            let base_logger = Arc::new(self.logger);
            let main_logger = PrefixLogger::new(&*base_logger, "main".to_string());

            main_logger.log(format_args!("ready for connections"));

            tokio::select! {
                _ = shutdown_receiver.wait_for_graceful_shutdown() => {
                    main_logger.log(format_args!("got shutdown signal"));
                }
                _ = async {
                    let mut request_counter: u16 = 0;
                    let shutdown_receiver = worker_shutdown_receiver.clone();

                    loop {
                        let permit = worker_semaphore.clone().acquire_owned().await.unwrap();

                        if self.verbose_logging {
                            main_logger.log(format_args!("waiting for next connection"));
                        }

                        let socket = match listener.accept().await {
                            Ok((socket, _)) => socket,
                            Err(e) => {
                                if self.verbose_logging {
                                    main_logger.log(format_args!("incoming connection failed: {}", e));
                                }

                                continue;
                            },
                        };

                        if self.verbose_logging {
                            main_logger.log(format_args!("got connection"));
                        }

                        request_counter = request_counter.overflowing_add(1).0;

                        let base_logger = base_logger.clone();
                        let mut shutdown_receiver  = shutdown_receiver.clone();
                        let request_index = request_counter;

                        spawn(async move {
                            let request_logger = PrefixLogger::new(&*base_logger, format!("request {}", request_index));

                            tokio::select! {
                                _ = async {
                                    if self.verbose_logging {
                                        handle_session(socket, request_index, &request_logger).await
                                    } else {
                                        handle_session(socket, request_index, &NullLogger {}).await
                                    };
                                } => {},
                                _ = shutdown_receiver.wait_for_forced_shutdown() => {
                                    if self.verbose_logging {
                                        request_logger.log(format_args!("interrupted"))
                                    }
                                },
                            };

                            drop(permit);
                            drop(shutdown_receiver);
                        });
                    }
                } => ()
            };
        });

        return Ok(shutdown_sender)
    }
}
