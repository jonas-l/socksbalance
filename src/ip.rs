use std::net::{IpAddr, Ipv6Addr};
use nix::ifaddrs::getifaddrs;
use nix::sys::socket::SockAddr;

pub fn get_public_ipv6_addresses() -> Vec<Ipv6Addr> {
    get_interface_ips()
        .filter_map(|address| match address {
            IpAddr::V6(address) => Some(address),
            _ => None,
        })
        .filter(|addr| is_public_ipv6(addr))
        .collect()
}

fn get_interface_ips() -> impl Iterator<Item = IpAddr> {
    getifaddrs()
        .unwrap()
        .filter_map(|ifaddr| ifaddr.address)
        .filter_map(|address| match address {
            SockAddr::Inet(address) => Some(address),
            _ => None,
        })
        .map(|addr| addr.ip().to_std())
}

fn is_public_ipv6(addr: &Ipv6Addr) -> bool {
    addr.octets()[0] >> 5 == 0b001
}
