use tokio::io::{AsyncRead, AsyncReadExt, Error, ErrorKind};

pub const VERSION: u8 = 5;

pub async fn read_version<T: AsyncRead>(stream: T) -> Result<(), Error> {
    tokio::pin!(stream);

    let version = stream.read_u8().await?;

    if version != VERSION {
        return Err(Error::new(ErrorKind::InvalidData, "invalid version number"))
    }

    Ok(())
}
