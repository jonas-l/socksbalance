use tokio::io::{AsyncRead, AsyncReadExt, Error, ErrorKind};
use super::SocksEndpoint;
use super::version::read_version;

#[derive(Debug)]
pub struct SocksClientRequest {
    pub command: SocksClientRequestCommand,
    pub endpoint: SocksEndpoint,
}

#[derive(Debug)]
pub enum SocksClientRequestCommand {
    Connect,
    Bind,
    UdpAssociate,
}

pub struct SocksClientRequestCommandInvalidError {}

impl SocksClientRequest {
    pub async fn decode<T: AsyncRead>(stream: T) -> Result<Self, Error> {
        tokio::pin!(stream);

        read_version(&mut stream).await?;

        let command = stream.read_u8().await?;
        let command = SocksClientRequestCommand::try_from(command)
            .or_else(|_| Err(Error::new(ErrorKind::InvalidData, "invalid command type")))?;

        let _reserved = stream.read_u8().await?;

        let endpoint = SocksEndpoint::decode(&mut stream).await?;

        Ok(Self { command, endpoint })
    }
}

impl TryFrom<u8> for SocksClientRequestCommand {
    type Error = SocksClientRequestCommandInvalidError;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        Ok(match value {
            1 => SocksClientRequestCommand::Connect,
            2 => SocksClientRequestCommand::Bind,
            3 => SocksClientRequestCommand::UdpAssociate,
            _ => return Err(Self::Error {}),
        })
    }
}
