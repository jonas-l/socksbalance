use tokio::io::{AsyncWrite, AsyncWriteExt, Error};
use super::version::VERSION;

#[derive(Debug)]
pub struct SocksServerHello {
    pub method: u8,
}

impl SocksServerHello {
    pub async fn encode<T: AsyncWrite>(&self, stream: T) -> Result<(), Error> {
        tokio::pin!(stream);

        stream.write_u8(VERSION).await?;
        stream.write_u8(self.method).await?;

        Ok(())
    }
}
