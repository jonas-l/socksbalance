use std::net::{IpAddr, Ipv4Addr, Ipv6Addr, SocketAddr};
use tokio::io::{AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt, Error, ErrorKind};
use tokio::net::lookup_host;

const TYPE_IP4: u8 = 1;
const TYPE_IP6: u8 = 4;
const TYPE_HOSTNAME: u8 = 3;

#[derive(Debug, Default)]
pub struct SocksEndpoint {
    pub host: SocksHost,
    pub port: u16,
}

#[derive(Debug)]
pub enum SocksHost {
    Ip4 ([u8; 4]),
    Ip6 ([u8; 16]),
    Hostname (String),
}

impl SocksEndpoint {
    pub async fn encode<T: AsyncWrite>(&self, stream: T) -> Result<(), Error> {
        tokio::pin!(stream);

        self.host.encode(&mut stream).await?;
        stream.write_u16(self.port).await?;

        Ok(())
    }

    pub async fn decode<T: AsyncRead>(stream: T) -> Result<Self, Error> {
        tokio::pin!(stream);

        let host = SocksHost::decode(&mut stream).await?;
        let port = stream.read_u16().await?;

        Ok(Self { host, port })
    }

    pub async fn to_socket_addrs(&self) -> Result<Vec<SocketAddr>, Error> {
        Ok(vec![
            SocketAddr::new(match &self.host {
                SocksHost::Ip4(address) => IpAddr::V4(Ipv4Addr::new(address[0], address[1], address[2], address[3])),
                SocksHost::Ip6(address) => IpAddr::V6(Ipv6Addr::new(
                    u16::from_be_bytes([address[0], address[1]]),
                    u16::from_be_bytes([address[2], address[3]]),
                    u16::from_be_bytes([address[4], address[5]]),
                    u16::from_be_bytes([address[6], address[7]]),
                    u16::from_be_bytes([address[8], address[9]]),
                    u16::from_be_bytes([address[10], address[11]]),
                    u16::from_be_bytes([address[12], address[13]]),
                    u16::from_be_bytes([address[14], address[15]])
                )),
                SocksHost::Hostname(hostname) => return Ok(
                    lookup_host(format!("{}:{}", hostname, self.port)).await?.collect::<Vec<SocketAddr>>()
                ),
            }, self.port)
        ])
    }
}

impl From<SocketAddr> for SocksEndpoint {
    fn from(addr: SocketAddr) -> Self {
        let host = addr.ip().into();
        let port = addr.port();

        Self { host, port }
    }
}

impl SocksHost {
    pub async fn encode<T: AsyncWrite>(&self, stream: T) -> Result<(), Error> {
        tokio::pin!(stream);

        match self {
            Self::Ip4 (address) => {
                stream.write_u8(TYPE_IP4).await?;
                stream.write_all(address).await?;
            },
            Self::Ip6 (address) => {
                stream.write_u8(TYPE_IP6).await?;
                stream.write_all(address).await?;
            },
            Self::Hostname (hostname) => {
                let hostname = hostname.as_bytes();
                let hostname_len: u8 = hostname.len().try_into()
                    .or_else(|_| Err(Error::new(ErrorKind::InvalidData, "hostname too long")))?;

                stream.write_u8(TYPE_HOSTNAME).await?;
                stream.write_u8(hostname_len).await?;
                stream.write_all(hostname).await?;
            },
        };

        Ok(())
    }

    pub async fn decode<T: AsyncRead>(stream: T) -> Result<Self, Error> {
        tokio::pin!(stream);

        let address_type = stream.read_u8().await?;

        Ok(match address_type {
            TYPE_IP4 => {
                let mut address = [0u8; 4];

                stream.read_exact(&mut address).await?;

                Self::Ip4(address)
            },
            TYPE_IP6 => {
                let mut address = [0u8; 16];

                stream.read_exact(&mut address).await?;

                Self::Ip6(address)
            },
            TYPE_HOSTNAME => {
                let hostname_length = stream.read_u8().await?;
                let mut hostname = vec![0u8; usize::from(hostname_length)];

                stream.read_exact(&mut hostname).await?;

                let hostname = String::from_utf8(hostname)
                    .or_else(|_| Err(Error::new(ErrorKind::InvalidData, "invalid hostname")))?;

                Self::Hostname(hostname)
            },
            _ => return Err(Error::new(ErrorKind::InvalidData, "invalid address type"))
        })
    }
}

impl Default for SocksHost {
    fn default() -> Self {
        Self::Ip4 ([0, 0, 0, 0])
    }
}

impl From<IpAddr> for SocksHost {
    fn from(ip: IpAddr) -> Self {
        match ip {
            IpAddr::V4(address) => SocksHost::Ip4(address.octets()),
            IpAddr::V6(address) => SocksHost::Ip6(address.octets()),
        }
    }
}
