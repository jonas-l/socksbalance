use tokio::io::{AsyncWrite, AsyncWriteExt, Error};
use super::SocksEndpoint;
use super::version::VERSION;

#[derive(Debug)]
pub struct SocksServerResponse {
    pub reply: SocksServerReply,
    pub endpoint: SocksEndpoint,
}

#[derive(Debug)]
#[allow(dead_code)] // not every possible value is actually used
pub enum SocksServerReply {
    Succeeded,
    GeneralServerFailure,
    ConnectionNotAllowedByRuleset,
    NetworkUnreachable,
    HostUnreachable,
    ConnectionRefused,
    TtlExpired,
    CommandNotSupported,
    AddressTypeNotSupported,
}

impl SocksServerResponse {
    pub async fn encode<T: AsyncWrite>(&self, stream: T) -> Result<(), Error> {
        tokio::pin!(stream);

        stream.write_u8(VERSION).await?;
        stream.write_u8(self.reply.to_u8()).await?;
        stream.write_u8(0).await?;
        self.endpoint.encode(&mut stream).await?;

        Ok(())
    }
}

impl SocksServerReply {
    fn to_u8(&self) -> u8 {
        match self {
            Self::Succeeded => 0,
            Self::GeneralServerFailure => 1,
            Self::ConnectionNotAllowedByRuleset => 2,
            Self::NetworkUnreachable => 3,
            Self::HostUnreachable => 4,
            Self::ConnectionRefused => 5,
            Self::TtlExpired => 6,
            Self::CommandNotSupported => 7,
            Self::AddressTypeNotSupported => 8,
        }
    }
}
