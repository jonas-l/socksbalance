use tokio::io::{AsyncRead, AsyncReadExt, Error};
use super::version::read_version;

#[derive(Debug)]
pub struct SocksClientHello {
    pub methods: Vec<u8>,
}

impl SocksClientHello {
    pub async fn decode<T: AsyncRead>(stream: T) -> Result<Self, Error> {
        tokio::pin!(stream);

        read_version(&mut stream).await?;

        let method_length = stream.read_u8().await?;

        let mut methods = vec![0u8; usize::from(method_length)];
        stream.read_exact(&mut methods).await?;

        Ok(Self {
            methods,
        })
    }
}
