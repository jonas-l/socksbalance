use super::Logger;
use std::fmt::Arguments;

pub struct ConsoleLogger {}

impl Logger for ConsoleLogger {
    fn log(&self, message: Arguments) {
        println!("{}", message)
    }
}
