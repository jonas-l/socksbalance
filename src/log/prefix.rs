use super::Logger;
use std::fmt::Arguments;

pub struct PrefixLogger<'a, T: Logger> {
    parent: &'a T,
    prefix: String,
}

impl<'a, T: Logger> PrefixLogger<'a, T> {
    pub fn new(parent: &'a T, prefix: String) -> Self {
        Self { parent, prefix }
    }
}

impl<'a, T: Logger> Logger for PrefixLogger<'a, T> {
    fn log(&self, message: Arguments) {
        self.parent.log(format_args!("{}: {}", self.prefix, message))
    }
}
