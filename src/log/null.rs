use super::Logger;
use std::fmt::Arguments;

pub struct NullLogger {}

impl Logger for NullLogger {
    fn log(&self, _message: Arguments) {/* do nothing */}
}
