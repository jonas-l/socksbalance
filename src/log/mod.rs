use std::fmt::Arguments;

mod console;
mod null;
mod prefix;

pub use console::ConsoleLogger;
pub use null::NullLogger;
pub use prefix::PrefixLogger;

pub trait Logger {
    fn log(&self, message: Arguments);
}
